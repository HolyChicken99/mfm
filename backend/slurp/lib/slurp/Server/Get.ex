defmodule Slurp.Server.Get do
  require Logger

  def get(url) do
    getHelper(Slurp.Utils.Helper.split(url))
  end

  defp getHelper({true, sub_url}) do
    [host_name, path] = sub_url
    Logger.debug("host name is #{host_name} , path is #{path}")

    case {:gen_tcp.connect(String.to_charlist(host_name), 80,
            active: false,
            mode: :binary
          )} do
      {{:ok, socket}} ->
        _ = :gen_tcp.send(socket, Slurp.Utils.Templater.request("get", host_name, path))
        {:ok, reply} = :gen_tcp.recv(socket, 0)
        :gen_tcp.close(socket)
        reply

        Enum.at(String.split(reply, "\r\n\r\n"), 0)

      _ ->
        IO.puts("Wrong url feed! \n Url should match `www.xyz.com/*` ")
    end
  end

  defp getHelper({false, _}) do
    IO.puts("Wrong host name or path given!!")
  end
end
